package tn.edu.esprit.sigma.fusion.youbay.utilities;

import javax.ejb.Remote;

@Remote
public interface SMSServiceRemote {
	public Boolean sendSMS(String num, String msg) ;

}
