package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity implementation class for Entity: HistoryOfViews
 *
 */
@Entity
@Table(name = "t_HistoryOfViews")
public class HistoryOfViews implements Serializable {

	// Embedded Id
	private HistoryOfViewsId historyOfViewsId;

	// "Regular" attribute

	String comment; // Optional, not used in production
	ClientType clientType;

	// Link attributes

	private Buyer buyer;
	private Product product;

	// "Virtual" attribute (actually exists in embedded class only) Date
	// Date dateTimeOfView;

	private static final long serialVersionUID = 1L;

	public HistoryOfViews() {
		super();
	}

	public HistoryOfViews(String comment, ClientType clientType, Buyer buyer,
			Product product, Date theDate) {
		super();
		this.historyOfViewsId = new HistoryOfViewsId(product.getProductId(),
				buyer.getYouBayUserId(), theDate);

		this.comment = comment;
		this.clientType = clientType;
		this.buyer = buyer;
		this.product = product;
	}

	@Column(length = 1000)
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public ClientType getClient() {
		return clientType;
	}

	public void setClient(ClientType clientType) {
		this.clientType = clientType;
	}

	@EmbeddedId
	public HistoryOfViewsId getHistoryOfViewsId() {
		return historyOfViewsId;
	}

	public void setHistoryOfViewsId(HistoryOfViewsId historyOfViewsId) {
		this.historyOfViewsId = historyOfViewsId;
	}

	public Date virtualGetDateTimeOfView() {
		return this.historyOfViewsId.getTheDate();
	}

	public void virtualSetDateTimeOfView(Date dateTimeOfView) {
		this.historyOfViewsId.setTheDate(dateTimeOfView);
	}

	@ManyToOne
	@JoinColumn(name = "BuyerId", referencedColumnName = "youBayUserId", updatable = false, insertable = false)
	public Buyer getBuyer() {
		return buyer;
	}

	public void setBuyer(Buyer buyer) {
		this.buyer = buyer;
	}

	@ManyToOne
	@JoinColumn(name = "ProductId", referencedColumnName = "productId", updatable = false, insertable = false)
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Override
	public String toString() {
		return "HistoryOfViews [historyOfViewsId=" + historyOfViewsId
				+ ", comment=" + comment + ", clientType=" + clientType
				+ ", buyer=" + buyer + ", product=" + product + "]";
	}

	
}
