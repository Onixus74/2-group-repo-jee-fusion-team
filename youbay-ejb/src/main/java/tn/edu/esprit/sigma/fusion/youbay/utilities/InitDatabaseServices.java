package tn.edu.esprit.sigma.fusion.youbay.utilities;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.AssistantItems;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Auction;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Buyer;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Category;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.ClientType;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.CustomizedAds;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.HistoryOfViews;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Manager;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.OrderAndReview;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Product;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.ProductHistory;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Seller;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.SpecialPromotion;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Subcategory;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.AssistantItemsServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.AuctionServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.BuyerServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.CategoryServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.CustomizedAdsServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.HistoryOfViewsServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.ManagerServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.OrderAndReviewServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.ProductHistoryServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.ProductServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.SellerServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.SpecialPromotionServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.SubcategoryServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.processing.EncryptionServicesLocal;

/**
 * Session Bean implementation class InitDatabase
 */

@Stateless
// @Startup
@SuppressWarnings("deprecation")
public class InitDatabaseServices implements InitDatabaseServicesRemote {

	@PersistenceContext
	EntityManager em;
	@EJB
	AssistantItemsServicesLocal assistantItemsServicesLocal;
	@EJB
	AuctionServicesLocal auctionServicesLocal;
	@EJB
	BuyerServicesLocal buyerServicesLocal;
	@EJB
	CategoryServicesLocal categoryServicesLocal;
	@EJB
	CustomizedAdsServicesLocal customizedAdsServicesLocal;
	@EJB
	HistoryOfViewsServicesLocal historyOfViewsServicesLocal;
	@EJB
	ManagerServicesLocal managerServicesLocal;
	@EJB
	OrderAndReviewServicesLocal orderAndReviewServicesLocal;
	@EJB
	ProductHistoryServicesLocal productHistoryServicesLocal;
	@EJB
	ProductServicesLocal productServicesLocal;
	@EJB
	SellerServicesLocal sellerServicesLocal;
	@EJB
	SpecialPromotionServicesLocal specialPromotionServicesLocal;
	@EJB
	SubcategoryServicesLocal subcategoryServicesLocal;
	@EJB
	EncryptionServicesLocal encryptionServices;

	/**
	 * Default constructor.
	 * 
	 */

	public InitDatabaseServices() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void truncateAllTables() {

		int deletedCount = 0;
		// we need to keep the truncate order
		System.out.println("=====> BEGIN truncateAllTables BEGIN <==================");
		deletedCount = em.createQuery("DELETE FROM Auction").executeUpdate();
		System.out.println(
				"------------------------------> truncateAllTables : deleted " + deletedCount + " Auction <-------");

		deletedCount = em.createQuery("DELETE FROM AssistantItems").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount + " AssistantItems <-------");

		// ATTENTION : Search for a solution to reset AUTO_INCREMENT
		// em.createNativeQuery("ALTER TABLE t_assistantitems AUTO_INCREMENT =
		// 1").executeUpdate();

		deletedCount = em.createQuery("DELETE FROM SpecialPromotion").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount + " SpecialPromotion <-------");
		deletedCount = em.createQuery("DELETE FROM ProductHistory").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount + " ProductHistory <-------");

		deletedCount = em.createQuery("DELETE FROM CustomizedAds").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount + " CustomizedAds <-------");

		deletedCount = em.createQuery("DELETE FROM HistoryOfViews").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount + " HistoryOfViews <-------");

		deletedCount = em.createQuery("DELETE FROM OrderAndReview").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount + " OrderAndReview <-------");

		deletedCount = em.createQuery("DELETE FROM Product").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount + " Product <-------");

		deletedCount = em.createQuery("DELETE FROM Subcategory").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount + " Subcategory <-------");

		deletedCount = em.createQuery("DELETE FROM Category").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount + " Sellers <-------");

		System.out.println("==================> END truncateAllTables END <==================");

		// we need to keep the truncate order
		deletedCount = em.createQuery("DELETE FROM Manager").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount + " Managers <-------");

		deletedCount = em.createQuery("DELETE FROM Buyer").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount + " Buyers <-------");

		deletedCount = em.createQuery("DELETE FROM Seller").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount + " Sellers <-------");

	}

	@Override
	public void addUsers() {
		/*
		 * Adds 9 USER 3 manager + 3 seller + 3 buyer
		 */

		String buyerPassword = encryptionServices.encryptString("buyer");
		String sellerPassword = encryptionServices.encryptString("seller");
		String managerPassword = encryptionServices.encryptString("manager");

		System.out.println("-----> Adding Users <-----");

		Buyer buyera1 = new Buyer("Houssem", "Sabbegh", "sabbegh@gmail.com", "21623215434", new Date("17/05/1991"),
				"tunisia", true, false, "", "1 Purchase;", true, 0f, "18 rue ESPRIT", "", "Ghazela", 0f, 15f);
		buyera1.setPassword(buyerPassword);

		Buyer buyera2 = new Buyer("Yassine", "Latiri", "pro@latiri.com", "21623165189", new Date("17/05/1991"),
				"tunisia", true, false, "", "5 Purchases;", true, 0f, "18 rue ESPRIT", "", "Ghazela", 0f, 15f);
		buyera2.setPassword(buyerPassword);

		buyerServicesLocal.addBuyer(buyera1);
		buyerServicesLocal.addBuyer(buyera2);

		Seller seller2 = new Seller("Houssem", "Sabbegh", "seller2@seller.com", "+21623843694", new Date(2015, 05, 18),
				"TUNISIA", true, false, "", "emptybadges", 1000f, 10f, "A fake description made for tests.", 10f, false,
				"logo string");
		seller2.setPassword(sellerPassword);

		Seller seller3 = new Seller("Tarek", "Latiri", "seller3@seller.com", "+21628154534", new Date(2015, 05, 17),
				"TUNISIA", true, false, "", "emptybadges", 1000f, 10f, "A fake description made for tests.", 10f, false,
				"logo string");
		seller3.setPassword(sellerPassword);

		sellerServicesLocal.addSeller(seller2);
		sellerServicesLocal.addSeller(seller3);

		Manager manager1 = new Manager("manager1 first name", "manager1 last name", "manager1@manager.com",
				"+216238785434", new Date(2015, 05, 18), "Tunisia", true, false, "", true, true, true, true, true);
		manager1.setPassword(managerPassword);

		Manager manager2 = new Manager("manager2 first name", "manager2 last name", "manager2@manager.com", "+21627434",
				new Date(2015, 05, 18), "Tunisia", true, false, "", true, true, true, true, true);
		manager2.setPassword(managerPassword);

		Manager manager3 = new Manager("manager3 first name", "manager3 last name", "manager3@manager.com",
				"+21623884215434", new Date(2015, 05, 18), "Tunisia", true, false, "", true, true, true, true, true);
		manager3.setPassword(managerPassword);

		managerServicesLocal.addManager(manager1);
		managerServicesLocal.addManager(manager2);
		managerServicesLocal.addManager(manager3);

		Buyer buyer1 = new Buyer("Yassine", "Latiri", "buyer1@buyer.com", "+216232134",
				new Date(2015 / 05 / 17), "TUNISIA", true, false, "", "no budget", true, 1547f, "lac II",
				"canada embassy", "tunisia", 54847954f, 41f);
		buyer1.setPassword(buyerPassword);

		Buyer buyer2 = new Buyer("Marwen", "Ounis", "buyer2@buyer.com", "+21625434",
				new Date(2015 / 05 / 17), "TUNISIA", true, false, "", "no budget", true, 1547f, "lac II",
				"canada embacy", "tunisia", 54847954f, 41f);
		buyer2.setPassword(buyerPassword);

		Buyer buyer3 = new Buyer("Houssem", "Sabbegh", "buyer3@buyer.com", "+21623215",
				new Date(2015 / 05 / 17), "TUNISIA", true, false, "", "no budget", true, 1547f, "lac II",
				"canada embacy", "tunisia", 54847954f, 41f);
		buyer3.setPassword(buyerPassword);

		buyerServicesLocal.addBuyer(buyer1);
		buyerServicesLocal.addBuyer(buyer2);
		buyerServicesLocal.addBuyer(buyer3);
	}

	@Override
	public void addAssistantItems() {
	}

	@Override
	public void addAuction() {
		Auction auction1 = new Auction(new Date(2015, 17, 05), new Date(2015, 17, 06), 10f);
		Auction auction2 = new Auction(new Date(2015, 17, 05), new Date(2015, 17, 06), 10f);
		Auction auction3 = new Auction(new Date(2015, 17, 05), new Date(2015, 17, 06), 10f);
		auctionServicesLocal.addAuction(auction1);
		auctionServicesLocal.addAuction(auction2);
		auctionServicesLocal.addAuction(auction3);
	}

	@Override
	public void addCategory() {
		Category category1 = new Category("Computers", 11);
		Category category2 = new Category("Electronics", 22);
		Category category3 = new Category("Accessories", 33);

		categoryServicesLocal.addCategory(category1);
		categoryServicesLocal.addCategory(category2);
		categoryServicesLocal.addCategory(category3);
	}

	@Override
	public void addCustomizedAds() {
		CustomizedAds customizedAds1 = new CustomizedAds(new Date(1991, 05, 17), new Date(2015, 05, 18), 254f,
				"customized ads 1 message", true, true);
		CustomizedAds customizedAds2 = new CustomizedAds(new Date(1991, 05, 17), new Date(2015, 05, 18), 254f,
				"customized ads 2 message", true, true);
		CustomizedAds customizedAds3 = new CustomizedAds(new Date(1991, 05, 17), new Date(2015, 05, 18), 254f,
				"customized ads 3 message", true, true);

		customizedAdsServicesLocal.addCustomizedAds(customizedAds1);
		customizedAdsServicesLocal.addCustomizedAds(customizedAds2);
		customizedAdsServicesLocal.addCustomizedAds(customizedAds3);

	}

	@Override
	public void addProduct() {

		addSubcategory();

		List<Subcategory> subcategories = subcategoryServicesLocal.findAllSubcategory();

		Subcategory computers_gaming = null, computers_desktop = null, computers_tablet = null,
				electronics_ereaders = null, electronics_hobby = null, electronics_tvs = null,
				accessories_backpack = null, accessories_keyboard = null, accessories_products = null;

		for (Subcategory c : subcategories) {

			if (c.getCategoryName().equals("Gaming Computer"))
				computers_gaming = c;
			if (c.getCategoryName().equals("Desktop Computer"))
				computers_desktop = c;
			if (c.getCategoryName().equals("Tablet"))
				computers_tablet = c;
			if (c.getCategoryName().equals("eReaders"))
				electronics_ereaders = c;
			if (c.getCategoryName().equals("Hobby Electronics"))
				electronics_hobby = c;
			if (c.getCategoryName().equals("TVs"))
				electronics_tvs = c;
			if (c.getCategoryName().equals("Computer Backpacks"))
				accessories_backpack = c;
			if (c.getCategoryName().equals("Mechanical Keyboards"))
				accessories_keyboard = c;
			if (c.getCategoryName().equals("Cleaning Products"))
				accessories_products = c;
		}

		Seller seller = new Seller("Yassine", "Latiri", "seller1@seller.com", "+21623256434", new Date(2015, 05, 18),
				"TUNISIA", true, false, "", "emptybadges", 1000f, 10f, "A fake description made for tests.", 10f, false,
				"logo string");
		seller.setPassword(encryptionServices.encryptString("seller"));
		sellerServicesLocal.addSeller(seller);

		if (computers_gaming != null) {
			Product producta1 = new Product("Ultra Fast Computer", "http://i.imgur.com/Zu4UvFl.png?2",
					"A Gaming Computer", 1000f, "marker1", false, false, 6);
			Product producta2 = new Product("Gaming Computer Tower", "http://imgur.com/rcvpNea.jpg",
					"A Gaming Computer", 1500f, "marker2 marker3", false, false, 5);

			producta1.setSubcategory(computers_gaming);
			producta2.setSubcategory(computers_gaming);

			producta1.setSeller(seller);
			producta2.setSeller(seller);

			productServicesLocal.addProduct(producta1);
			productServicesLocal.addProduct(producta2);

			Product product1 = new Product("Alienware AW17R3", "http://i.imgur.com/voMpvxT.jpg?1",
					"Intel Quad Core i7-6700HQ 2.6 GHz Processor 16 GB DDR4 1 TB HDD + 256 GB SSD Storage; Optical Drive Not included 17.3 Inch FHD (1920 x 1080 pixels) LED-lit Truelife Screen Windows 10 Operating System; Epic Silver Chassis",
					1000f, "marker1", false, false, 5);

			Product product2 = new Product("ASUS ROG GL552VW", "http://i.imgur.com/5lSX9lF.jpg?1",
					"Powerful 6th-generation Intel Core i7-6700HQ 2.6GHz. 16GB RAM; 1TB 7200RPM Storage. 15.6 inches Matte IPS FHD display. 1920 by 1080 resolution. NVIDIA GeForce GTX 960M gaming graphic card. GameFirst III technology prioritizes the flow of the game data across your network. Feature the latest USB3.1 Type-C port for more convenient connectivity options. 100 solder points for durability and solid-feel, one piece keyboard with 1.8mm of key travel, gives you responsive keystrokes when typing and entering commands; The slide-off upgrade panel offers instant access, fit an M.2 SSD, a bigger hard drive or upgrade memory.",
					1000f, "marker1 marker2", false, false, 5);

			Product product3 = new Product("Razer Blade 14 QHD", "http://imgur.com/rcvpNea.jpg",
					"Intel Core i7-4720HQ 2.6 GHz 16.0 GB DDR3L SDRAM 512.0 GB 1.0 rpm 512.0 GB Solid-State Drive 14.0-Inch Screen, NVIDIA GeForce GTX 970M Windows 10",
					1000f, "marker3 marker2", false, false, 5);

			product1.setSubcategory(computers_gaming);
			product2.setSubcategory(computers_gaming);
			product3.setSubcategory(computers_gaming);

			product1.setSeller(seller);
			product2.setSeller(seller);
			product3.setSeller(seller);

			productServicesLocal.addProduct(product1);
			productServicesLocal.addProduct(product2);
			productServicesLocal.addProduct(product3);

		}

		if (computers_desktop != null) {
			Product product1 = new Product("Desktop Computer 1", "http://imgur.com/40NAHkA.jpg?1", "An Acer Desktop",
					1000f, "marker3", false, false, 5);
			Product product2 = new Product("Desktop Computer 2", "http://imgur.com/q6eC589.jpg?1",
					"HP Desktop Computer", 1000f, "", false, false, 5);

			Product product3 = new Product("Dell OptiPlex 755 Desktop", "http://i.imgur.com/ZFSWqB0.jpg?1",
					"17 LCD Monitor (models may very), Keyboard, Mouse, Keyboard, and cables all included", 1000f,
					"marker2 marker3", false, false, 5);

			Product product4 = new Product("HP All in One", "http://i.imgur.com/kciP7lj.jpg?1",
					"This Certified Refurbished product is tested and certified to look and work like new. The refurbishing process includes functionality testing, basic cleaning, inspection, and repackaging. The product ships with all relevant accessories, a minimum 90-day warranty, and may arrive in a generic box. Only select sellers who maintain a high performance bar may offer Certified Refurbished products on Amazon.com 19.5 Inch HD+ 1600x900 LED Backlit Display (Non-Touch), Intel Dual Core Celeron J1800 2.41 GHz up to 2.58 GHz, 4GB DDR3-1600MHz RAM, 500GB 7200 RPM HDD; Slim-tray SuperMulti DVD Burner, 10/100/1000Base-T Ethernet, 802.11b/g/n Wireless LAN, Webcam, Memory Card Reader, 3 USB 2.0, 1 USB 3.0.Windows 8 Professional included. Free uprade/ downgrade to Windows 10 Professional or Windows 7 Professional at your choice.",
					1000f, "marker1", false, false, 5);

			Product product5 = new Product("Apple MB323LL/A iMac", "http://i.imgur.com/379e5WX.jpg?1",
					"2.8GHz quad-core Intel Core i5 Turbo Boost up to 3.3GHz 8GB Onboard Memory (Configurable to 16GB) 1TB (5400-rpm) Hard Drive Intel Iris Pro Graphics 6200 21.5 LED-backlit IPS Display 1920 x 1080 Resolution",
					1000f, "marker2", false, false, 5);

			product1.setSubcategory(computers_desktop);
			product2.setSubcategory(computers_desktop);
			product3.setSubcategory(computers_desktop);
			product4.setSubcategory(computers_desktop);
			product5.setSubcategory(computers_desktop);

			product1.setSeller(seller);
			product2.setSeller(seller);
			product3.setSeller(seller);
			product4.setSeller(seller);
			product5.setSeller(seller);

			productServicesLocal.addProduct(product1);
			productServicesLocal.addProduct(product2);
			productServicesLocal.addProduct(product3);
			productServicesLocal.addProduct(product4);
			productServicesLocal.addProduct(product5);
		}

		if (computers_tablet != null) {
			Product product1 = new Product("Samsung Galaxy Tab 4", "http://i.imgur.com/J2aGEj0.jpg?1",
					"Android 4.4 Kit Kat OS, 1.2 GHz quad-core processor 8 GB Flash Memory, 1.5 GB RAM Memory WXGA Display (1280x800 Resolution) 32GB of memory available through a microSD slot and 50GB of free Dropbox storage Comes with over $300 of free content and services Wi-Fi Tablet, Android 4.4 Kit Kat OS, 1.2 GHz quad-core processor 8 GB Internal Memory, 1.5 GB RAM Memory",
					1000f, "marker1", false, false, 5);

			// Product product2 = new Product("Dragon Touch Y88X Plus 7",
			// "http://i.imgur.com/74Rpk1C.jpg?1",
			// "Stunning Viewing Experience: Casting off the original normal TN
			// screen, this tablet sports the 178 degree view IPS display which
			// presents best graphics performance to you. Packed with the
			// upgraded camera and SmartColor display technology of the A33
			// chip, you can take dazzling photos and videos. Contents become
			// incredibly detailed and razor sharp on this little thing. The
			// world is so close and so clear to you, and changes under your
			// fingertips. Ultrafast and Fluid Performance: Featuring the
			// incredibly powerful Quad Core A33 chip, this Y88X Plus model
			// delivers ultrafast multitasking speed and rivals many 7 inch
			// tablets. Everything is more responsive and fluid. Combined with
			// KitKat 4.4 OS, 8GB memory (expandable with SD card) and its
			// super-portable size, Y88X Plus will prove to be a necessary for
			// travel of choices. Advanced Wireless, Worry-free Mobility: Stay
			// connected on the go with the built-in Wi-Fi. Wherever there is
			// Wi-Fi, you can link yourself to the world. Beyond that, the
			// built-in Bluetooth transforms your Y88X+ into powerful
			// multi-media. You can easily pair it with keyboards, speakers and
			// other Bluetooth devices, making your work and life much more
			// convenient and enjoyable. Y88X+ has the Google Play store
			// pre-loaded, providing you access to countless apps that make the
			// tablet whatever you want it to be. More than 500,000 apps out
			// there awaiting your summon. 1 YEAR Limited warranty! on-time
			// customer service/tech support, frequently updated firmware,
			// guarantees customer satisfactory. Feel free to contact us
			// whenever a question pops up in your head, we are always here to
			// help",
			// 1000f, "marker3", false, false, 5);

			Product product3 = new Product("Chromo Inc 7 Tablet", "http://i.imgur.com/5VjhKIa.jpg?1",
					"Beautiful 7 Touchscreen Tablet for Games, Entertainment and Education The Tablet comes preloaded with popular Apps like Netflix, Skype, GooglePlay, Chrome and more The Chromo Inc Tablet has 4GB of internal memory and is expandable up to 32GB of memory via Micro SD Card The Tablet runs on the Android 4.4 Operating System and comes with popular useful features like Front Facing Camera (for Photos and Webcam/Skype,) WiFi (Internet Service required,) Speech and Sound Recording and more NEW FOR 2015 HOLIDAY SEASON: The chromo tablet has been updated and certified to TUV Quality and Safety standards. TUV is an internationally recognized independent QC and safety standards firm. Besides giving greater peace of mind for the consumer, these standards are perfect for schools and any other organization requiring rigorous safety certification.",
					1000f, "marker2", false, false, 5);

			product1.setSubcategory(computers_tablet);
			// product2.setSubcategory(computers_tablet);
			product3.setSubcategory(computers_tablet);

			product1.setSeller(seller);
			// product2.setSeller(seller);
			product3.setSeller(seller);

			productServicesLocal.addProduct(product1);
			// productServicesLocal.addProduct(product2);
			productServicesLocal.addProduct(product3);
		}

		if (electronics_ereaders != null) {
			Product product1 = new Product("Kindle eReader", "http://i.imgur.com/GaD9gfs.jpg?1",
					"A Certified Refurbished Kindle is refurbished, tested, and certified to look and work like new Touchscreen display that reads like real paper-no screen glare, even in bright sunlightExclusive Kindle features-now includes Goodreads integration, Kindle FreeTime, Vocabulary Builder, Word Wise, and more Double the on-device storage-holds thousands of books Page turns fly with a 20% faster processor Lighter than a paperback-fits in your pocket Battery lasts weeks, not hours",
					100f, "marker1", false, false, 6);

			Product product2 = new Product("Fire, 7", "http://i.imgur.com/nL3D8C3.jpg?1",
					"Beautiful 7 IPS display (171 ppi / 1024 x 600) and fast 1.3 GHz quad-core processor. Rear and front-facing cameras. All-new Amazon Underground, a one-of-a-kind app store experience where over $10,000 in apps, games and even in-app items are actually free - including extra lives, unlocked levels, unlimited add-on packs and more Enjoy more than 38 million movies, TV shows, songs, books, apps and games 8 GB of internal storage. Free unlimited cloud storage for all Amazon content and photos taken with Fire devices. Add a microSD card for up to 128 GB of additional storage. Updated user interface - Fire OS 5 designed for quick access to your apps and content plus personalized recommendations that make it easy to discover new favorites Up to 7 hours of reading, surfing the web, watching videos, and listening to music Stay connected with fast web browsing, email, and calendar support",
					210f, "marker2", false, false, 7);

			Product product3 = new Product("Ematic EB106-RB 7", "http://i.imgur.com/GaD9gfs.jpg?1",
					"7 inch Screen 256MB DDR2 Ram Mini USB 2.0 Headphone MicroSD card slot for the expansion of memory. Download and play your favorite music.",
					100f, "marker3", false, false, 6);

			product1.setSubcategory(electronics_ereaders);
			product2.setSubcategory(electronics_ereaders);
			product3.setSubcategory(electronics_ereaders);

			product1.setSeller(seller);
			product2.setSeller(seller);
			product3.setSeller(seller);

			productServicesLocal.addProduct(product1);
			productServicesLocal.addProduct(product2);
			productServicesLocal.addProduct(product3);
		}

		if (electronics_hobby != null) {
			Product product1 = new Product("Arduino Uno Kit", "http://i.imgur.com/kG88Dgv.jpg?",
					"This is the ulitamate Arduino Uno kit enough to get you started with hundreds of arduino projects Includes the Arduino Uno Rev3 Board Bread Board -- Holder -- Jumper Wires -- USB Cable -- LEDs -- DC Motor -- Small Servo -- Relay Includes a total of over 190 electronic parts and components 72 page full color Instruction Manual",
					180f, "marker1", false, false, 5);

			Product product2 = new Product("CanaKit Raspberry Pi 2", "http://i.imgur.com/CkoEwYW.jpg?1",
					"Includes Raspberry Pi 2 (RPi2) Model B Quad-Core 900 MHz 1 GB RAM 8 GB Micro SD Card (Class 10) pre-loaded with NOOBS, CanaKit WiFi Adapter, CanaKit 2.5A Power Supply with 5 feet Micro USB Cable and Noise Filter (UL Listed) High Quality Raspberry Pi 2 Case, Premium Quality 6.5 feet HDMI Cable, Heat Sink, CanaKit Full Color Quick-Start Guide CanaKit GPIO to Breadboard Interface Board, Ribbon Cable, Breadboard, Jumper Wires, GPIO and Resistor Colors Quick Reference Cards RGB LED, 8 x LEDs (Blue/Red/Yellow/Green), 15 x Resistors, 2 x Push Button Switches",
					320f, "marker2", false, false, 5);

			product1.setSubcategory(electronics_hobby);
			product2.setSubcategory(electronics_hobby);

			product1.setSeller(seller);
			product2.setSeller(seller);

			productServicesLocal.addProduct(product1);
			productServicesLocal.addProduct(product2);
		}

		if (electronics_tvs != null) {
			Product product1 = new Product("Sony KDL48R510C 48", "http://i.imgur.com/oyKSept.jpg?1",
					"Refresh Rate: 60Hz (Native); Motionflow XR 120 (Effective) Backlight: LED (Edge-Lit) Smart Functionality: Yes Dimensions (W x H x D): TV without stand: 42.9  x 21.1  x 2.6 , TV with stand: 42.9  x 26.5  x 8.0  Inputs: 2 HDMI, 2 USB Accessories Included: Remote, Table Top Stand",
					500f, "marker1", false, false, 5);

			Product product2 = new Product("TCL 32D2700 32-Inch", "http://i.imgur.com/wyf9agT.jpg?1",
					"Refresh Rate: 60Hz (Native) Backlight: LED (Full Array) Smart Functionality: No Dimensions (W x H x D): TV without stand: 29  x 18.7  x 3.2 , TV with stand: 29  x 17  x 7.8'' Inputs: 2 HDMI, 1 USB, RF, Composite & Component (shared), Digital Audio Out (coaxial) Accessories Included: Remote w/ batteries, detachable power cord",
					400f, "marker2", false, false, 5);

			Product product3 = new Product("Vizio E24-C1 24", "http://i.imgur.com/dPSt2kf.jpg?1",
					"Refresh Rate: 60Hz (Effective) Backlight: LED (Edge-Lit) Smart Functionality: Yes - Built-in Wi-Fi Dimensions (W x H x D): TV without stand: 21.6  x 13.8  x 2.1 , TV with stand: 21.6  x 14.6  x 4.4  uts: 1 HDMI, 1 USB Ports Accessories Included: Remote",
					400f, "marker3", false, false, 5);

			product1.setSubcategory(electronics_tvs);
			product2.setSubcategory(electronics_tvs);
			product3.setSubcategory(electronics_tvs);

			product1.setSeller(seller);
			product2.setSeller(seller);
			product3.setSeller(seller);

			productServicesLocal.addProduct(product1);
			productServicesLocal.addProduct(product2);
			productServicesLocal.addProduct(product3);
		}

		if (accessories_backpack != null) {
			Product product1 = new Product("Samsonite Classic", "http://i.imgur.com/72zC8JV.jpg?1",
					"Samsonite Classic Carrying Case (Backpack) for 15.6 Notebook - Black - Shock Resistant Interior - Ballistic Fabric - Checkpoint Friendly - Handle, Shoulder Strap Samsonite 55937-1041",
					1000f, "marker1", false, false, 5);

			Product product2 = new Product("Cabod(TM) Attractive", "http://i.imgur.com/eFzM0Iy.jpg?1",
					"Comfortable and adjustable shoulder straps with well-padded back and shoulder cushions for comfortable wear Durable and water-resistant, comes in black with blue stripes and gray with yellow stripes Specially fitted, organized pockets to hold calculator, mobile phone, notepads, pens and pencils Multiple pockets of various sizes for all school paraphernalia � Outside, zippered accessory pockets Sturdy straps for hand-carry and secure hanging � Inside pockets can hold magazines, documents, notebooks and folders",
					1000f, "marker2", false, false, 5);
			Product product3 = new Product("Macbook Pro 15 Sleeve", "http://i.imgur.com/qPOQL9t.jpg?1",
					"Unique Internal Anti-Slip Design Prevents Ultrabook Laptop Notebook Macbook Pro 15 with Retina From Dropping and Slipping. Invisible Handle with two pockets design, Sleeve Pouch Turns into Briefcase Handbag in minutes. Soft and reversible neoprene can protect your Macbook Pro from impact, suddenly drop, scratch, dust, oil and etc. Slim and lightweight - does not bulk your macbook up and can easily slide into the briefcase, backpack, or other bag. Suitable for most popular 15-15.6 inch Ultrabook Laptop Notebook including Apple Macbook Pro 15, Macbook Pro 15 with Retina, Asus X551MA-SX018H, Toshiba Satellite C55-A-1UC, Lenovo G500, Acer Aspire E1-530, Acer Aspire E1-572,Dell Inspiron 15.6-inch Laptop, Asus X551CA and etc.",
					1000f, "marker3", false, false, 5);

			product1.setSubcategory(accessories_backpack);
			product2.setSubcategory(accessories_backpack);
			product3.setSubcategory(accessories_backpack);

			product1.setSeller(seller);
			product2.setSeller(seller);
			product3.setSeller(seller);

			productServicesLocal.addProduct(product1);
			productServicesLocal.addProduct(product2);
			productServicesLocal.addProduct(product3);
		}

		if (accessories_keyboard != null) {
			Product product1 = new Product("CM Storm QuickFire Rapid", "http://i.imgur.com/qvvsguh.jpg?1",
					"Mechanical BROWN CHERRY MX Switches Windows keys disabled in Game mode Extra key caps bundled (with key puller) Laser-marked key caps Anti-Ghosting NKRO in PS/2 mode Removable braided USB cable with cable routing Compact design without numpad section provides extra space for your mouse and allows your shoulders to be comfortably positioned",
					1000f, "marker2", false, false, 5);
			Product product2 = new Product("Logitech  G710 Blue", "http://i.imgur.com/bu1D634.jpg?1",
					"Tactile, high-speed keys: Cherry MX Blue Switches deliver gaming-grade responsiveness with high-speed,  clicky  tactile feedback Adjustable dual-zone backlighting: Adjust the brightness of WASD/arrow keys independently from the rest of the keyboard so you can easily find any key-even in low light 6 programmable G-keys: Configure up to 18 unique functions per game, including single key presses, complex macros or intricate LUA scripts High-performance gaming keys: 110 anti-ghosting keys and 26-key rollover help you make the right moves with flawless precision; one-touch media keys let you instantly control volume, mute and media playback Compatibility: Works with Windows 8.1, Windows 8, Windows 7 or Mac OS X",
					1000f, "marker1", false, false, 5);
			Product product3 = new Product("Rosewill RK-6000", "http://i.imgur.com/sBReTqa.jpg?1",
					"Fully programmable keys Laser printed keycaps Rubber coated WASD and arrow keys Braided cable gold USB connector Switch life time : 20 million Cycles Keyboard Interface: USB Normal Keys: 104",
					1000f, "marker3", false, false, 5);

			product1.setSubcategory(accessories_keyboard);
			product2.setSubcategory(accessories_keyboard);
			product3.setSubcategory(accessories_keyboard);

			product1.setSeller(seller);
			product2.setSeller(seller);
			product3.setSeller(seller);

			productServicesLocal.addProduct(product1);
			productServicesLocal.addProduct(product2);
			productServicesLocal.addProduct(product3);

		}

		if (accessories_products != null) {
			Product product1 = new Product("USB Fan", "http://i.imgur.com/Q1WCAPH.png?1", "To cool your laptop.", 10f,
					"marker1", false, false, 5);
			Product product2 = new Product("Thermal grease", "http://i.imgur.com/bxgQow6.jpg?1", "HP Laptop", 1000f,
					"marker2", false, false, 5);

			product1.setSubcategory(accessories_products);
			product2.setSubcategory(accessories_products);

			product1.setSeller(seller);
			product2.setSeller(seller);

			productServicesLocal.addProduct(product1);
			productServicesLocal.addProduct(product2);
		}

	}

	@Override
	public void addProductHistory() {
		ProductHistory productHistory1 = new ProductHistory(new Date(1478, 05, 17), "product history1 name",
				"product history1 image URL", "short description", "productMainDescriptionHistory", 10f,
				"subcategoryAdditionalValuesHistory", 10);
		ProductHistory productHistory2 = new ProductHistory(new Date(1478, 05, 17), "product history2 name",
				"product history2 image URL", "short description", "productMainDescriptionHistory", 10f,
				"subcategoryAdditionalValuesHistory", 10);
		ProductHistory productHistory3 = new ProductHistory(new Date(1478, 05, 17), "product history3 name",
				"product history3 image URL", "short description", "productMainDescriptionHistory", 10f,
				"subcategoryAdditionalValuesHistory", 10);
		productHistoryServicesLocal.addProductHistory(productHistory1);
		productHistoryServicesLocal.addProductHistory(productHistory2);
		productHistoryServicesLocal.addProductHistory(productHistory3);

	}

	@Override
	public void addSpecialPromotion() {
		SpecialPromotion promotion1 = new SpecialPromotion(new Date(2014, 05, 17), new Date(2014, 05, 17), false,
				"deal1 ShortDescription", "deal1 Description", 5f);
		SpecialPromotion promotion2 = new SpecialPromotion(new Date(2014, 05, 17), new Date(2014, 05, 17), false,
				"deal2 ShortDescription", "deal2 Description", 5f);
		SpecialPromotion promotion3 = new SpecialPromotion(new Date(2014, 05, 17), new Date(2014, 05, 17), false,
				"deal3 ShortDescription", "deal3 Description", 5f);
		specialPromotionServicesLocal.addSpecialPromotion(promotion1);
		specialPromotionServicesLocal.addSpecialPromotion(promotion2);
		specialPromotionServicesLocal.addSpecialPromotion(promotion3);

	}

	@Override
	public void addSubcategory() {

		addCategory();

		List<Category> categories = categoryServicesLocal.findAllCategory();

		Category computers = null, electronics = null, accessories = null;

		for (Category c : categories) {
			if (c.getCategoryName().equals("Computers"))
				computers = c;

			if (c.getCategoryName().equals("Electronics"))
				electronics = c;

			if (c.getCategoryName().equals("Accessories"))
				accessories = c;
		}

		if (computers != null) {
			Subcategory subcategory1 = new Subcategory("Gaming Computer", 10, "", true,
					"http://i.imgur.com/atTgE60.jpg?1");
			Subcategory subcategory2 = new Subcategory("Desktop Computer", 20, "", true,
					"http://i.imgur.com/vHgIqhz.jpg?1");
			Subcategory subcategory3 = new Subcategory("Tablet", 30, "", true, "http://i.imgur.com/Lb194Fz.png?1");

			subcategory1.setCategory(computers);
			subcategory2.setCategory(computers);
			subcategory3.setCategory(computers);

			subcategoryServicesLocal.addSubcategory(subcategory1);
			subcategoryServicesLocal.addSubcategory(subcategory2);
			subcategoryServicesLocal.addSubcategory(subcategory3);

			AssistantItems GamingComputer1 = new AssistantItems(10, "Do you want an Nvida graphic card", "", "", "", "");
			GamingComputer1.setSubcategory(subcategory1);
			assistantItemsServicesLocal.addAssistantItems(GamingComputer1);
			
			AssistantItems GamingComputer2 = new AssistantItems(20, "Are you looking for a very powerful gaming rig", "", "", "", "");
			GamingComputer2.setSubcategory(subcategory1);
			assistantItemsServicesLocal.addAssistantItems(GamingComputer2);
			
			AssistantItems GamingComputer3 = new AssistantItems(30, "Is a metal case necessary", "", "", "", "");
			GamingComputer3.setSubcategory(subcategory1);
			assistantItemsServicesLocal.addAssistantItems(GamingComputer3);
			
			AssistantItems DesktopComputer1 = new AssistantItems(10, "Do you want to buy a PC with Windows OS", "", "", "", "");
			DesktopComputer1.setSubcategory(subcategory2);
			assistantItemsServicesLocal.addAssistantItems(DesktopComputer1);
			
			AssistantItems DesktopComputer2 = new AssistantItems(20, "Do you need a lot (more than 8 Gb) of RAM", "", "", "", "");
			DesktopComputer2.setSubcategory(subcategory2);
			assistantItemsServicesLocal.addAssistantItems(DesktopComputer2);
			
			AssistantItems DesktopComputer3 = new AssistantItems(30, "Is this computer to be used for multimedia production", "", "", "", "");
			DesktopComputer3.setSubcategory(subcategory2);
			assistantItemsServicesLocal.addAssistantItems(DesktopComputer3);
			
			AssistantItems TabletComputer1 = new AssistantItems(10, "Do you want a big (10 inch) screen", "", "", "", "");
			TabletComputer1.setSubcategory(subcategory3);
			assistantItemsServicesLocal.addAssistantItems(TabletComputer1);
			
			AssistantItems TabletComputer2 = new AssistantItems(20, "Do you want an Android tablet", "", "", "", "");
			TabletComputer2.setSubcategory(subcategory3);
			assistantItemsServicesLocal.addAssistantItems(TabletComputer2);
			
			AssistantItems TabletComputer3 = new AssistantItems(30, "Can you afford a high-end tablet", "", "", "", "");
			TabletComputer3.setSubcategory(subcategory3);
			assistantItemsServicesLocal.addAssistantItems(TabletComputer3);
		}

		if (electronics != null) {
			Subcategory subcategory4 = new Subcategory("eReaders", 10, "", true, "http://i.imgur.com/k6IO9VM.jpg?1");
			Subcategory subcategory5 = new Subcategory("Hobby Electronics", 20, "", true,
					"http://i.imgur.com/GryIpA8.jpg?1");
			Subcategory subcategory6 = new Subcategory("TVs", 30, "", true, "http://i.imgur.com/up8Zva5.jpg?1");

			subcategory4.setCategory(electronics);
			subcategory5.setCategory(electronics);
			subcategory6.setCategory(electronics);

			subcategoryServicesLocal.addSubcategory(subcategory4);
			subcategoryServicesLocal.addSubcategory(subcategory5);
			subcategoryServicesLocal.addSubcategory(subcategory6);
			
			
			AssistantItems EreaderComputer1 = new AssistantItems(10, "Do you want an e-ink eReader", "", "", "", "");
			EreaderComputer1.setSubcategory(subcategory4);
			assistantItemsServicesLocal.addAssistantItems(EreaderComputer1);
			
			AssistantItems EreaderComputer2 = new AssistantItems(20, "Do you want an Amazon tablet", "", "", "", "");
			EreaderComputer2.setSubcategory(subcategory4);
			assistantItemsServicesLocal.addAssistantItems(EreaderComputer2);
			
			AssistantItems EreaderComputer3 = new AssistantItems(30, "Does you need epub support", "", "", "", "");
			EreaderComputer3.setSubcategory(subcategory4);
			assistantItemsServicesLocal.addAssistantItems(EreaderComputer3);
			
			AssistantItems HobbyComputer1 = new AssistantItems(10, "Are you looking for a starter kit", "", "", "", "");
			HobbyComputer1.setSubcategory(subcategory5);
			assistantItemsServicesLocal.addAssistantItems(HobbyComputer1);
			
			AssistantItems HobbyComputer2 = new AssistantItems(20, "Are you qualified in Electrical Engineering", "", "", "", "");
			HobbyComputer2.setSubcategory(subcategory5);
			assistantItemsServicesLocal.addAssistantItems(HobbyComputer2);
			
			AssistantItems HobbyComputer3 = new AssistantItems(30, "Are you willing to buy a high end kit", "", "", "", "");
			HobbyComputer3.setSubcategory(subcategory5);
			assistantItemsServicesLocal.addAssistantItems(HobbyComputer3);
			
			AssistantItems TVComputer1 = new AssistantItems(10, "Are you looking for a big (40 inch) TV", "", "", "", "");
			TVComputer1.setSubcategory(subcategory6);
			assistantItemsServicesLocal.addAssistantItems(TVComputer1);
			
			AssistantItems TVComputer2 = new AssistantItems(20, "Is True HD a requiment", "", "", "", "");
			TVComputer2.setSubcategory(subcategory6);
			assistantItemsServicesLocal.addAssistantItems(TVComputer2);
			
			AssistantItems TVComputer3 = new AssistantItems(30, "Does this TV need to support HDMI", "", "", "", "");
			TVComputer3.setSubcategory(subcategory6);
			assistantItemsServicesLocal.addAssistantItems(TVComputer3);
			
		}

		if (accessories != null) {
			Subcategory subcategory7 = new Subcategory("Computer Backpacks", 10, "", true,
					"http://i.imgur.com/tvOjm9m.jpg?2");
			Subcategory subcategory8 = new Subcategory("Mechanical Keyboards", 20, "", true,
					"http://i.imgur.com/QQyjd7Z.jpg?1");
			Subcategory subcategory9 = new Subcategory("Cleaning Products", 30, "", true,
					"http://i.imgur.com/s5KKDki.jpg?1");

			subcategory7.setCategory(accessories);
			subcategory8.setCategory(accessories);
			subcategory9.setCategory(accessories);

			subcategoryServicesLocal.addSubcategory(subcategory7);
			subcategoryServicesLocal.addSubcategory(subcategory8);
			subcategoryServicesLocal.addSubcategory(subcategory9);
			
			AssistantItems BackPackComputer1 = new AssistantItems(10, "Do you a backpack (rather than a messenger bag)", "", "", "", "");
			BackPackComputer1.setSubcategory(subcategory7);
			assistantItemsServicesLocal.addAssistantItems(BackPackComputer1);
			
			AssistantItems BackPackComputer2 = new AssistantItems(20, "Are you looking for a high-end item", "", "", "", "");
			BackPackComputer2.setSubcategory(subcategory7);
			assistantItemsServicesLocal.addAssistantItems(BackPackComputer2);
			
			AssistantItems BackPackComputer3 = new AssistantItems(30, "Is a shoulderhung required", "", "", "", "");
			BackPackComputer3.setSubcategory(subcategory7);
			assistantItemsServicesLocal.addAssistantItems(BackPackComputer3);
			
			AssistantItems KeyboardComputer1 = new AssistantItems(10, "Do you need strong (black MX) keys", "", "", "", "");
			KeyboardComputer1.setSubcategory(subcategory8);
			assistantItemsServicesLocal.addAssistantItems(KeyboardComputer1);
			
			AssistantItems KeyboardComputer2 = new AssistantItems(20, "Are multimedia keys required", "", "", "", "");
			KeyboardComputer2.setSubcategory(subcategory8);
			assistantItemsServicesLocal.addAssistantItems(KeyboardComputer2);
			
			AssistantItems KeyboardComputer3 = new AssistantItems(30, "Is an extra USB port necessary", "", "", "", "");
			KeyboardComputer3.setSubcategory(subcategory8);
			assistantItemsServicesLocal.addAssistantItems(KeyboardComputer3);
			
			AssistantItems CleaningComputer1 = new AssistantItems(10, "Is this component for a desktop computer", "", "", "", "");
			CleaningComputer1.setSubcategory(subcategory9);
			assistantItemsServicesLocal.addAssistantItems(CleaningComputer1);
			
			AssistantItems CleaningComputer2 = new AssistantItems(20, "Does it need to be ISO 9001 certified", "", "", "", "");
			CleaningComputer2.setSubcategory(subcategory9);
			assistantItemsServicesLocal.addAssistantItems(CleaningComputer2);
			
			AssistantItems CleaningComputer3 = new AssistantItems(30, "Are you willing to pay for high end product", "", "", "", "");
			CleaningComputer3.setSubcategory(subcategory9);
			assistantItemsServicesLocal.addAssistantItems(CleaningComputer3);
			
		}
	}

	@Override
	public void addHistoryOfViews() {
		/* Always run this method AFTER running addUsers and addProdutcs ! */

		List<Buyer> listBuyers = buyerServicesLocal.findAllBuyer();
		List<Product> listProducts = productServicesLocal.findAllProduct();

		HistoryOfViews historyItem1 = new HistoryOfViews("Added automatically", ClientType.other, listBuyers.get(0),
				listProducts.get(0), new Date());
		HistoryOfViews historyItem2 = new HistoryOfViews("Added automatically", ClientType.other, listBuyers.get(0),
				listProducts.get(1), new Date());
		HistoryOfViews historyItem3 = new HistoryOfViews("Added automatically", ClientType.other, listBuyers.get(1),
				listProducts.get(0), new Date());

		historyOfViewsServicesLocal.addHistoryOfViews(historyItem1);
		historyOfViewsServicesLocal.addHistoryOfViews(historyItem2);
		historyOfViewsServicesLocal.addHistoryOfViews(historyItem3);

	}

	@Override
	public void addOrderAndReview() {
		/* Always run this method AFTER running addUsers and addProdutcs ! */

		List<Buyer> listBuyers = buyerServicesLocal.findAllBuyer();
		List<Product> listProducts = productServicesLocal.findAllProduct();

		OrderAndReview order1 = new OrderAndReview(15f, "Please package the items carefuly !", false, false, new Date(),
				false, false, null, null, null, listBuyers.get(0), listProducts.get(0));

		OrderAndReview order2 = new OrderAndReview(15f, "Please package the items carefuly !", false, false, new Date(),
				false, false, null, null, null, listBuyers.get(1), listProducts.get(1));

		orderAndReviewServicesLocal.addOrderAndReview(order1);
		orderAndReviewServicesLocal.addOrderAndReview(order2);
	}

	@Override
	// @PostConstruct
	public void populateDatabase() {

		System.out.println("==================> INTITIALISING DATABASE ! <==================");

		/* Always run these methods FIRST */
		addUsers();
		addAssistantItems();
		addAuction();
		addCustomizedAds();
		addProduct();
		addProductHistory();
		addSpecialPromotion();

		/* Always run these methods LAST */
		addHistoryOfViews();
		addOrderAndReview();

	}

}
