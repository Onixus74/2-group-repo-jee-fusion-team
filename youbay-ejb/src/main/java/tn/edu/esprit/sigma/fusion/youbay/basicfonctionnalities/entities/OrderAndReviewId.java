package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Embeddable;

/**
 * Entity implementation class for Entity: OrderAndReviewId
 *
 */
@Embeddable
public class OrderAndReviewId implements Serializable {

	private Long ProductId;
	private Long BuyerId;
	private Date orderDate;

	public Date getTheDate() {
		return orderDate;
	}

	public void setTheDate(Date theDate) {
		this.orderDate = theDate;
	}

	private static final long serialVersionUID = 1L;

	public OrderAndReviewId() {
		super();
	}

	public OrderAndReviewId(Long productId, Long buyerId, Date theDate) {
		super();
		this.ProductId = productId;
		this.BuyerId = buyerId;
		this.orderDate = theDate;
	}

	public Long getProductId() {
		return this.ProductId;
	}

	public void setProductId(Long ProductId) {
		this.ProductId = ProductId;
	}

	public Long getBuyerId() {
		return this.BuyerId;
	}

	public void setBuyerId(Long BuyerId) {
		this.BuyerId = BuyerId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((BuyerId == null) ? 0 : BuyerId.hashCode());
		result = prime * result
				+ ((ProductId == null) ? 0 : ProductId.hashCode());
		result = prime * result + ((orderDate == null) ? 0 : orderDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderAndReviewId other = (OrderAndReviewId) obj;
		if (BuyerId == null) {
			if (other.BuyerId != null)
				return false;
		} else if (!BuyerId.equals(other.BuyerId))
			return false;
		if (ProductId == null) {
			if (other.ProductId != null)
				return false;
		} else if (!ProductId.equals(other.ProductId))
			return false;
		if (orderDate == null) {
			if (other.orderDate != null)
				return false;
		} else if (!orderDate.equals(other.orderDate))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "OrderAndReviewId [ProductId=" + ProductId + ", BuyerId="
				+ BuyerId + ", orderDate=" + orderDate + "]";
	}


}
