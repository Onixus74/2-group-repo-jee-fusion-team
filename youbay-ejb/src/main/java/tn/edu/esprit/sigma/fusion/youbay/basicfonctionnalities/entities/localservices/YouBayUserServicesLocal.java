package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices;

import javax.ejb.Local;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.YouBayUser;

@Local
public interface YouBayUserServicesLocal {

	YouBayUser findYouBayUserByEmail(String email);
	YouBayUser findYouBayUserByEmail(String email, String password);

}
