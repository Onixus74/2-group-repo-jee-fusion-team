package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices;

import java.util.List;

import javax.ejb.Remote;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Subcategory;

@Remote
public interface SubcategoryServicesRemote {

	Boolean addSubcategory(Subcategory theSubcategory);

	Long addSubcategoryAndReturnId(Subcategory theSubcategory);

	Subcategory findSubcategoryById(Long theId);

	Boolean updateSubcategory(Subcategory theSubcategory);

	Boolean deleteSubcategory(Subcategory theSubcategory);

	Boolean deleteSubcategoryById(Long theId);

	List<Subcategory> findAllSubcategory();

}
