package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices;

import java.util.List;

import javax.ejb.Remote;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.AssistantItems;

@Remote
public interface AssistantItemsServicesRemote {
	Boolean addAssistantItems(AssistantItems theAssistantItems);

	Long addAssistantItemsAndReturnId(AssistantItems theAssistantItems);

	AssistantItems findAssistantItemsById(Long theId);

	Boolean updateAssistantItems(AssistantItems theAssistantItems);

	Boolean deleteAssistantItems(AssistantItems theAssistantItems);

	Boolean deleteAssistantItemsById(Long theId);

	List<AssistantItems> findAllAssistantItems();

}
