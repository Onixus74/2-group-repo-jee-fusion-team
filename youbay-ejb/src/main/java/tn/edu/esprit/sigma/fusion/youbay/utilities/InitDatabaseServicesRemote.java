package tn.edu.esprit.sigma.fusion.youbay.utilities;

import javax.ejb.Remote;

@Remote
public interface InitDatabaseServicesRemote {

	public void truncateAllTables();

	public void populateDatabase();

	public void addUsers();

	public void addAssistantItems();

	public void addAuction();

	public void addCategory();

	public void addCustomizedAds();

	public void addHistoryOfViews();

	public void addOrderAndReview();

	public void addProduct();

	public void addProductHistory();

	public void addSpecialPromotion();

	public void addSubcategory();
}
