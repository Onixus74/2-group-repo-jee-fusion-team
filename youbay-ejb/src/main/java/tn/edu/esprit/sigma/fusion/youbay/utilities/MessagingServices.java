package tn.edu.esprit.sigma.fusion.youbay.utilities;

import java.util.Properties;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Session Bean implementation class MessagingServices
 */
@Stateless
@Asynchronous
public class MessagingServices implements MessagingServicesRemote, MessagingServicesLocal {

	public MessagingServices() {
		// TODO Auto-generated constructor stub
	}

	@Asynchronous
	public void sendEmail(String to, String subject, String msg) {
		final String userName = "h3y2devdur@gmail.com";
		final String password = "votezottavi";
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(userName, password);
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("h3y2devdur@gmail.com"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
			message.setSubject(subject);
			message.setText(msg);

			Transport.send(message);

			// System.out.println(">>>>>>>>>>>>>>>>>>>> EJB ASYNCHRONOUS ; IL
			// EST ; "+new
			// Date());

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

	}
}
