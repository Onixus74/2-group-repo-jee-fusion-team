package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices;

import java.util.List;

import javax.ejb.Remote;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Seller;

@Remote
public interface SellerServicesRemote {

	Boolean addSeller(Seller theSeller);

	Long addSellerAndReturnId(Seller theSeller);

	Seller findSellerById(Long theId);

	Boolean updateSeller(Seller theSeller);

	Boolean deleteSeller(Seller theSeller);

	Boolean deleteSellerById(Long theId);

	List<Seller> findAllSeller();

}
