package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.servicesimplementation;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Category;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.CategoryServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.CategoryServicesRemote;

/**
 * Session Bean implementation class CategoryServices
 */
@Stateless
public class CategoryServices implements CategoryServicesRemote,
		CategoryServicesLocal {

	@PersistenceContext
	EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public CategoryServices() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Boolean addCategory(Category theCategory) {
		Boolean b = false;
		try {
			entityManager.persist(theCategory);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Category findCategoryById(Long theId) {
		return entityManager.find(Category.class, theId);
	}

	@Override
	public Boolean updateCategory(Category theCategory) {
		boolean b = false;
		try {
			entityManager.merge(theCategory);
			b = true;
		} catch (Exception e) {
		}
		return b;

	}

	@Override
	public Boolean deleteCategory(Category theCategory) {
		Boolean b = false;
		try {
			theCategory = findCategoryById(theCategory.getCategoryId());
			entityManager.remove(theCategory);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Boolean deleteCategoryById(Long theId) {
		Boolean b = false;
		try {
			entityManager.remove(findCategoryById(theId));
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Category> findAllCategory() {
		Query query = entityManager.createQuery("select e from Category e ");
		return query.getResultList();
	}

	@Override
	public Long addCategoryAndReturnId(Category theCategory) {

		try {
			
			entityManager.persist(theCategory);
			entityManager.flush();
			Long theID = theCategory.getCategoryId();
			
			return theID;
		} catch (Exception e)
		{
		}
		return null;
		
	}

}
