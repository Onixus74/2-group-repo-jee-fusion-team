package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices;

import java.util.List;

import javax.ejb.Local;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Product;

@Local
public interface ProductServicesLocal {

	Boolean addProduct(Product theProduct);
	
	Long addProductAndReturnId(Product theProduct);

	Product findProductById(Long theId);

	Boolean updateProduct(Product theProduct);

	Boolean deleteProduct(Product theProduct);

	Boolean deleteProductById(Long theId);

	List<Product> findAllProduct();
}
