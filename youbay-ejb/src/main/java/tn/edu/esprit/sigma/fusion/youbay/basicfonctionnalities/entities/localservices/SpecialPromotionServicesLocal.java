package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices;

import java.util.List;

import javax.ejb.Local;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.SpecialPromotion;

@Local
public interface SpecialPromotionServicesLocal {

	Boolean addSpecialPromotion(SpecialPromotion theSpecialPromotion);

	Long addSpecialPromotionAndReturnId(SpecialPromotion theSpecialPromotion);

	SpecialPromotion findSpecialPromotionById(Long theId);

	Boolean updateSpecialPromotion(SpecialPromotion theSpecialPromotion);

	Boolean deleteSpecialPromotion(SpecialPromotion theSpecialPromotion);

	Boolean deleteSpecialPromotionById(Long theId);

	List<SpecialPromotion> findAllSpecialPromotion();

}
