package tn.edu.esprit.sigma.fusion.youbaywar.managedbeans.products;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Product;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Seller;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.ProductServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.SellerServicesLocal;
import tn.edu.esprit.sigma.fusion.youbaywar.managedbeans.utilities.IdentityBean;

@ManagedBean
@ViewScoped
public class SellerProductsBean {

	@ManagedProperty("#{identityBean}")
	IdentityBean identityBean;

	@EJB
	private SellerServicesLocal sellerServicesLocal;

	@EJB
	private ProductServicesLocal productServicesLocal;

	private Seller seller;

	List<Product> products = new ArrayList<Product>();

	private Boolean displyTable = false;

	private Product productChosen = new Product();

	public String doCreateProduct() {
		productChosen.setQuantityAvailable(5);
		productServicesLocal.addProduct(productChosen);
		displyTable = false;
		return "";
	}

	public String doDeleteProduct() {
		productServicesLocal.deleteProduct(productChosen);
		productChosen = new Product();
		displyTable = false;
		return "";
	}

	public void doSelect() {
		displyTable = true;
	}

	public void doDisplayTable() {
		displyTable = true;
	}

	public List<Product> getProducts() {
		seller = sellerServicesLocal.findSellerById(identityBean.getYouBayUserfound().getYouBayUserId());
		products = productServicesLocal.findAllProduct();
		List<Product> products2 = new ArrayList<>();
		for (Product product : products) {
			if (product.getQuantityAvailable() == 5) {
				products2.add(product);
			}
		}
		return products2;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public IdentityBean getIdentityBean() {
		return identityBean;
	}

	public void setIdentityBean(IdentityBean identityBean) {
		this.identityBean = identityBean;
	}

	public Seller getSeller() {
		return seller;
	}

	public void setSeller(Seller seller) {
		this.seller = seller;
	}

	public Boolean getDisplyTable() {
		return displyTable;
	}

	public void setDisplyTable(Boolean displyTable) {
		this.displyTable = displyTable;
	}

	public Product getProductChosen() {
		return productChosen;
	}

	public void setProductChosen(Product productChosen) {
		this.productChosen = productChosen;
	}

}
