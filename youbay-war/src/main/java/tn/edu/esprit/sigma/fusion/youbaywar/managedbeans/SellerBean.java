package tn.edu.esprit.sigma.fusion.youbaywar.managedbeans;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Seller;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.SellerServicesLocal;
import tn.edu.esprit.sigma.fusion.youbaywar.managedbeans.utilities.IdentityBean;

@ManagedBean
@ViewScoped
public class SellerBean {
	@EJB
	private SellerServicesLocal sellerServicesLocal;
	
	@ManagedProperty("#{identityBean}")
	IdentityBean identityBean;
	
	
	
	private Seller seller ;

	public Seller getSeller() {
		seller=(Seller)identityBean.getYouBayUserfound();
		return seller;
	}

	public void setSeller(Seller seller) {
		this.seller = seller;
	}

	public IdentityBean getIdentityBean() {
		return identityBean;
	}

	public void setIdentityBean(IdentityBean identityBean) {
		this.identityBean = identityBean;
	}
	
	
	
}
