//package tn.edu.esprit.sigma.fusion.youbaywar.convertor;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.faces.component.UIComponent;
//import javax.faces.context.FacesContext;
//import javax.faces.convert.Converter;
//import javax.faces.convert.FacesConverter;
//import javax.inject.Inject;
//
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Category;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.CategoryServicesLocal;
//
//@FacesConverter("categoryConverter")
//public class CategoryConverter implements Converter {
//
//	@Inject
//	private CategoryServicesLocal categoryServicesLocal;
//
//	@Override
//	public Object getAsObject(FacesContext context, UIComponent component, String value) {
//		Category eqCategory = null;
//		if (!value.trim().equals("")) {
//			List<Category> categories = new ArrayList<Category>();
//			categories = categoryServicesLocal.findAllCategory();
//			for (Category c : categories) {
//				if (c.getCategoryName().trim().equals(value.trim()))
//					return c;
//			}
//		}
//		return null;
//	}
//
//	@Override
//	public String getAsString(FacesContext context, UIComponent component, Object value) {
//		String eqString = null;
//
//		if (value == null || value.equals("")) {
//			eqString = "";
//		} else {
//			eqString = ((Category) value).getCategoryName();
//		}
//		return eqString;
//	}
//
//}
