package tn.edu.esprit.sigma.fusion.youbaywar.managedbeans.utilities;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped
public class Button {

	
	
	public String redirectToLogin() {
		return "/login?faces-redirect=true";
	}
}
