package tn.edu.esprit.sigma.fusion.youbaywar.managedbeans.products;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.AssistantItems;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Category;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Product;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Subcategory;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.AssistantItemsServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.CategoryServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.ProductServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.SubcategoryServicesLocal;

@ManagedBean(name = "productsBean")
@SessionScoped
public class ProductsBean {
	private List<Product> products;
	private List<Product> customProducts;
	private List<Category> categories;
	private List<Subcategory> subcategories; // Subcategories of current
												// selected category
	private List<Subcategory> activeSubcategories; // activeSubcategories -
													// current selected
													// subcategory or
													// subcategories

	private String selectedCategoryId;
	private String selectedSubcategoryId;
	private Integer focusedQuestion;
	private String question1, question2, question3;
	private Boolean answer1, answer2, answer3;
	private Subcategory focusedSubcategory;

	@EJB
	private ProductServicesLocal productServicesLocal;
	@EJB
	private SubcategoryServicesLocal subcategoryServicesLocal;
	@EJB
	private CategoryServicesLocal categoryServicesLocal;
	@EJB
	private AssistantItemsServicesLocal assistantItemsServicesLocal;

	public ProductsBean() {
	}

	@PostConstruct
	public void InitProductsBean() {

		this.selectedCategoryId = null;
		this.selectedSubcategoryId = null;
		this.focusedSubcategory = null;
		this.focusedQuestion = null;
		this.answer1 = this.answer2 = this.answer3 = false;

		this.products = new ArrayList<Product>();
		this.customProducts = new ArrayList<Product>();
		this.categories = new ArrayList<Category>();
		this.subcategories = new ArrayList<Subcategory>();
		this.activeSubcategories = new ArrayList<Subcategory>();

		this.products = productServicesLocal.findAllProduct();
		
		for (int i= 0; i <3; i++)
		{
			this.customProducts.add(products.get(ThreadLocalRandom.current().nextInt(0, products.size()-1)));
		}
		
		this.categories = categoryServicesLocal.findAllCategory();
		this.subcategories = subcategoryServicesLocal.findAllSubcategory();
	}

	public void categoryIsSelected() {

		this.answer1 = this.answer2 = this.answer3 = false;
		this.question1 = this.question2 = this.question3 = null;
		
		selectedSubcategoryId = null;
		focusedSubcategory = null;
		this.focusedQuestion = null;

		if (selectedCategoryId == null) {

			// All Categories are selected, reset subcategories to all

			subcategories = subcategoryServicesLocal.findAllSubcategory();
			activeSubcategories = subcategoryServicesLocal.findAllSubcategory();

		} else {

			// One category is selected, set subcategories to its child
			// subcategories

			subcategories = new ArrayList<Subcategory>();
			activeSubcategories = new ArrayList<Subcategory>();

			for (Subcategory s : subcategoryServicesLocal.findAllSubcategory()) {
				if (Long.valueOf(selectedCategoryId) == s.getCategory().getCategoryId()) {
					subcategories.add(s);
					activeSubcategories.add(s);
				}
			}

		}

		return;
	}

	public void subcategoryIsSelected() {

		this.answer1 = this.answer2 = this.answer3 = false;
		this.question1 = this.question2 = this.question3 = null;

		if (selectedSubcategoryId == null) {
			categoryIsSelected();
		}

		else {

			focusedSubcategory = subcategoryServicesLocal.findSubcategoryById(Long.valueOf(selectedSubcategoryId));
			activeSubcategories = new ArrayList<Subcategory>();
			activeSubcategories.add(focusedSubcategory);
			this.focusedQuestion = 1;

			List<AssistantItems> aItems = assistantItemsServicesLocal.findAllAssistantItems();

			this.question1 = this.question2 = this.question3 = null;
			
			for (AssistantItems a : aItems) {

				if (focusedSubcategory.getSubcategoryId() == a.getSubcategory().getSubcategoryId()) {
					
					if (question1 == null) {
						question1 = new String(a.getQuestionText());
					}

					else if (question2 == null) {
						question2 = new String(a.getQuestionText());
					} else if (question3 == null) {
						question3 = new String(a.getQuestionText());
					}
				}
				
			}

		}
	}

	public void yesAnswerIsSelected() {
		System.out.println("-------- YES Answer Selected, question was : " + focusedQuestion + " -------- ");

		if (focusedQuestion == 1)
			answer1 = true;
		if (focusedQuestion == 2)
			answer2 = true;
		if (focusedQuestion == 3)
			answer3 = true;

		focusedQuestion++;
		return;
	}

	public void noAnswerIsSelected() {
		System.out.println("-------- NO Answer Selected, question was  : " + focusedQuestion + " -------- ");

		if (focusedQuestion == 1)
			answer1 = false;
		if (focusedQuestion == 2)
			answer2 = false;
		if (focusedQuestion == 3)
			answer3 = false;

		focusedQuestion++;
		return;
	}

	public void easySearchSubmit() {
		System.out.println("------------------------- EasySearchSubmit() ! -------------------------");
		System.out.println(" -------- CATEGORY SELECTED : " + selectedCategoryId);
		System.out.println(" -------- SUBCATEGORY OF CATEGORY : " + selectedSubcategoryId);
		System.out.println(" -------- SUBCATEGORIES SELECTED : " + subcategories);
		System.out.println(" -------- ACTIVE SUBCATEGORIES : " + activeSubcategories);
		System.out.println(" -------- ACTIVE PRODUCTS : " + products);
		System.out.println(" -------- FOCUSED QUESTION : " + focusedQuestion);
		System.out.println(
				" -------- QUESTIONS : 1 -> " + question1 + " | 2 -> " + question2 + " | 3 -> " + question3 + ".");
		System.out.println(" -------- ANSWERS : 1 -> " + answer1 + " | 2 -> " + answer2 + " | 3 -> " + answer3 + ".");

		products = new ArrayList<Product>();
		for (Product p : productServicesLocal.findAllProduct()) {
			for (Subcategory s : activeSubcategories) {
				if (p.getSubcategory().getSubcategoryId() == s.getSubcategoryId()) {

					if (answer1) {
						if (!p.getSubcategoryAttributesAndValues().contains("marker1")) {
							continue;
						}

					}

					if (answer2) {
						if (!p.getSubcategoryAttributesAndValues().contains("marker2")) {
							continue;
						}
					}

					if (answer3) {
						if (!p.getSubcategoryAttributesAndValues().contains("marker3")) {
							continue;
						}
					}

					products.add(p);
				}
			}
		}

		return;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public List<Subcategory> getSubcategories() {
		return subcategories;
	}

	public void setSubcategories(List<Subcategory> subcategories) {
		this.subcategories = subcategories;
	}

	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	public String getSelectedCategoryId() {
		return selectedCategoryId;
	}

	public void setSelectedCategoryId(String selectedCategoryId) {
		this.selectedCategoryId = selectedCategoryId;
	}

	public String getSelectedSubcategoryId() {
		return selectedSubcategoryId;
	}

	public void setSelectedSubcategoryId(String selectedSubcategoryId) {
		this.selectedSubcategoryId = selectedSubcategoryId;
	}

	public Subcategory getFocusedSubcategory() {
		return focusedSubcategory;
	}

	public void setFocusedSubcategory(Subcategory focusedSubcategory) {
		this.focusedSubcategory = focusedSubcategory;
	}

	public Integer getFocusedQuestion() {
		return focusedQuestion;
	}

	public void setFocusedQuestion(Integer focusedQuestion) {
		this.focusedQuestion = focusedQuestion;
	}

	public String getQuestion1() {
		return question1;
	}

	public void setQuestion1(String question1) {
		this.question1 = question1;
	}

	public String getQuestion2() {
		return question2;
	}

	public void setQuestion2(String question2) {
		this.question2 = question2;
	}

	public String getQuestion3() {
		return question3;
	}

	public void setQuestion3(String question3) {
		this.question3 = question3;
	}

	public Boolean getAnswer1() {
		return answer1;
	}

	public List<Product> getCustomProducts() {
		return customProducts;
	}

	public void setCustomProducts(List<Product> customProducts) {
		this.customProducts = customProducts;
	}
	
	

}