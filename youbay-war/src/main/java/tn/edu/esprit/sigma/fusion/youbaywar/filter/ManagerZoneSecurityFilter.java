package tn.edu.esprit.sigma.fusion.youbaywar.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tn.edu.esprit.sigma.fusion.youbaywar.managedbeans.utilities.IdentityBean;



@WebFilter("/manager/*")
public class ManagerZoneSecurityFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		IdentityBean identityBean = (IdentityBean) req.getSession().getAttribute("identityBean");
		Boolean letGo = false;
		if (identityBean != null && identityBean.getYouBayUserfound() != null && identityBean.hasRole("manager"))
			letGo = true;

		if (letGo) {
			chain.doFilter(request, response);
		} else {
			resp.sendRedirect(req.getContextPath() + "/login.jsf");
		}

	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

}
