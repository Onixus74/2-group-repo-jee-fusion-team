/*
 * 
 * Author Marwen
 */

package tn.edu.esprit.sigma.fusion.youbayclient.processingtest;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Buyer;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Seller;
import tn.edu.esprit.sigma.fusion.youbayclient.basicfonctionnalities.delegate.InitDatabaseServicesDelegate;
import tn.edu.esprit.sigma.fusion.youbayclient.processing.delegate.SubscriptionServicesDelegate;

public class SubscriptionServicesDelegateTest {
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		InitDatabaseServicesDelegate.doTruncateAllTables();

		InitDatabaseServicesDelegate.doPopulateDatabase();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDoSubscribeSeller() {
		Seller seller = new Seller();
		seller.setFirstName("Marwen");
		seller.setLastName("Ounis");
		seller.setIsActive(false);
		seller.setEmail("marwen.ounis@esprit.tn");
		SubscriptionServicesDelegate.doSubscribeSeller(seller);

	}

	@Test
	public void testDoSubscribeBuyer() {
		Buyer buyer = new Buyer();
		buyer.setFirstName("Hedi");
		buyer.setLastName("Mejri");
		buyer.setIsActive(false);
		buyer.setEmail("marwen.ounis@esprit.tn");
		SubscriptionServicesDelegate.doSubscribeBuyer(buyer);

	}
}
