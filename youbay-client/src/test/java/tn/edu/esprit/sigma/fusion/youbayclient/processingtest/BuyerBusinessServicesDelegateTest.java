package tn.edu.esprit.sigma.fusion.youbayclient.processingtest;

import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Buyer;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.OrderAndReview;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Product;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Seller;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Subcategory;
import tn.edu.esprit.sigma.fusion.youbayclient.basicfonctionnalities.delegate.BasicFonctionnalitiesDelegate;
import tn.edu.esprit.sigma.fusion.youbayclient.basicfonctionnalities.delegate.InitDatabaseServicesDelegate;
import tn.edu.esprit.sigma.fusion.youbayclient.processing.delegate.BuyerBusinessServicesDelegate;

public class BuyerBusinessServicesDelegateTest {
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
//		InitDatabaseServicesDelegate.doTruncateAllTables();

		InitDatabaseServicesDelegate.doPopulateDatabase();
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void doAddProductToCart (){
		
		List<Buyer> list = BasicFonctionnalitiesDelegate.doFindAllBuyer();
		long theId = list.get(0).getYouBayUserId();
		Buyer buyer = BasicFonctionnalitiesDelegate.doFindBuyerById(theId);
			
		List<Seller> listSeller = BasicFonctionnalitiesDelegate.doFindAllSeller();
		long idSeller = listSeller.get(0).getYouBayUserId();
		Seller seller = BasicFonctionnalitiesDelegate.doFindSellerById(idSeller);
		
		
		List<Product> listProduct = BasicFonctionnalitiesDelegate.doFindAllProduct();
		long idProduct = listProduct.get(0).getProductId();
		Product product = BasicFonctionnalitiesDelegate.doFindProductById(idProduct);
		
		System.out.println(seller.getYouBayUserId());
		
		BuyerBusinessServicesDelegate.doAddProductToCart(buyer, product);
		

		
	}
	
	@Test
	public void doConfirmOrder() {
		List<Buyer> listBuyers = BasicFonctionnalitiesDelegate.doFindAllBuyer();
		List<Product> listProducts = BasicFonctionnalitiesDelegate
				.doFindAllProduct();

		OrderAndReview order1 = new OrderAndReview(15f,
				"Please package the items carefuly !", false, false,
				new Date(), false, false, null, null, null, listBuyers.get(0),
				listProducts.get(0));
		BuyerBusinessServicesDelegate.doConfirmOrder(listBuyers.get(0), order1);
	}
	
	@Test
	public void doCompareProducts() {
		List<Product> listProduct = BasicFonctionnalitiesDelegate.doFindAllProduct();
		long idProduct1 = listProduct.get(0).getProductId();
		Product product = BasicFonctionnalitiesDelegate.doFindProductById(idProduct1);
		
		long idProduct2 = listProduct.get(0).getProductId();
		Product product2 = BasicFonctionnalitiesDelegate.doFindProductById(idProduct2);
		BuyerBusinessServicesDelegate.doCompareProducts(product, product2);

	}
	
	@Test
	public void doFindProductsAdvanced(){
		Subcategory subCategory = new Subcategory();

		subCategory.setCategoryName("Informatique");

		BasicFonctionnalitiesDelegate.doAddSubcategory(subCategory);
		BuyerBusinessServicesDelegate.doFindProductsAdvanced(1000.0f, 5.0f,subCategory, "ram");
	}
	
	
}
