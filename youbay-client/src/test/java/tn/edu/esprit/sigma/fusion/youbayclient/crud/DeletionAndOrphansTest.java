/* Author :  Yassine */

package tn.edu.esprit.sigma.fusion.youbayclient.crud;

import java.util.Date;
import java.util.Random;
import java.util.UUID;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Buyer;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.ClientType;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.HistoryOfViews;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.OrderAndReview;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Product;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Seller;
import tn.edu.esprit.sigma.fusion.youbayclient.basicfonctionnalities.delegate.BasicFonctionnalitiesDelegate;
import tn.edu.esprit.sigma.fusion.youbayclient.basicfonctionnalities.delegate.InitDatabaseServicesDelegate;
@SuppressWarnings("deprecation")
public class DeletionAndOrphansTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		InitDatabaseServicesDelegate.doTruncateAllTables();
	}

	@After
	public void tearDown() throws Exception {
	}

	public String randomString() {
		return UUID.randomUUID().toString();
	}

	public Float randomFloat() {
		Random random = new Random();
		return random.nextFloat() * (10000);
	}

	@Test
	public void testDeleteBuyerWithHistoryAndOrders() {

		/* Create and Persist a Buyer and a Product */

		Buyer newBuyer = new Buyer("Cascade First Name", "Cascade Last Name", "CascadeEmail@email.com", "21623000000",
				new Date("17/05/1991"), "Tunisia", true, false, "", "", true, 0f, "No where street", "", "No City", 0f,
				0f);

		Buyer anotherBuyer = new Buyer("Cascade Control First Name", "Cascade Control Last Name",
				"CascadeControlEmail@email.com", "21623000001", new Date("17/05/1991"), "Tunisia", true, false, "", "",
				true, 0f, "No where street", "", "No City", 0f, 0f);

		Product newProduct = new Product("Cascade product name", "Cascade product image url",
				"Cascade product description", 10f, "Cascade product description", true, true, 10);

		Long buyerId = BasicFonctionnalitiesDelegate.doAddBuyerAndReturnId(newBuyer);
		Long referenceBuyerId = BasicFonctionnalitiesDelegate.doAddBuyerAndReturnId(anotherBuyer);
		Long productId = BasicFonctionnalitiesDelegate.doAddProductAndReturnId(newProduct);

		Buyer buyer = BasicFonctionnalitiesDelegate.doFindBuyerById(buyerId);
		Buyer referenceBuyer = BasicFonctionnalitiesDelegate.doFindBuyerById(referenceBuyerId);
		Product product = BasicFonctionnalitiesDelegate.doFindProductById(productId);

		/* Create and persist some history of views */

		Date now = new Date(), later = new Date();
		later.setTime(later.getTime() + 1000);

		HistoryOfViews historyItem1 = new HistoryOfViews(randomString(), ClientType.other, buyer, product, now);
		HistoryOfViews historyItem2 = new HistoryOfViews(randomString(), ClientType.other, buyer, product, later);
		HistoryOfViews referenceHistoryItem = new HistoryOfViews(randomString(), ClientType.other, referenceBuyer,
				product, now);

		BasicFonctionnalitiesDelegate.doAddHistoryOfViews(historyItem1);
		BasicFonctionnalitiesDelegate.doAddHistoryOfViews(historyItem2);
		BasicFonctionnalitiesDelegate.doAddHistoryOfViews(referenceHistoryItem);

		/* Create and persist some orders */

		OrderAndReview order1 = new OrderAndReview(15f, "Cascade package the items carefuly !", false, false, now,
				false, false, null, null, null, buyer, product);

		OrderAndReview order2 = new OrderAndReview(15f, "Cascade send in a strong cardoard box !", false, false, later,
				false, false, null, null, null, buyer, product);

		OrderAndReview referenceOrder = new OrderAndReview(15f, "Cascade please declare amount on customs form !",
				false, false, now, false, false, null, null, null, referenceBuyer, product);

		BasicFonctionnalitiesDelegate.doAddOrderAndReview(order1);
		BasicFonctionnalitiesDelegate.doAddOrderAndReview(order2);
		BasicFonctionnalitiesDelegate.doAddOrderAndReview(referenceOrder);

		/*
		 * try and delete the buyer without deleting the association classes
		 * first
		 */

		BasicFonctionnalitiesDelegate.doDeleteBuyer(buyer);
	}

	
	@Test
	public void testDeleteProductWithHistoryAndOrders() {

		/* Create and Persist a Buyer and a Product */

		Buyer newBuyer = new Buyer("Cascade First Name", "Cascade Last Name", "CascadeEmail@email.com", "21623000000",
				new Date("17/05/1991"), "Tunisia", true, false, "", "", true, 0f, "No where street", "", "No City", 0f,
				0f);

		Product newProduct = new Product("Cascade product name", "Cascade product image url",
				"Cascade product description", 10f, "Cascade product description", true, true, 10);

		Product anotherProduct = new Product("Cascade Control product name", "Cascade Control product image url",
				"Cascade Control product description", 10f, "Cascade Control product description", true, true, 10);

		Long buyerId = BasicFonctionnalitiesDelegate.doAddBuyerAndReturnId(newBuyer);
		Long productId = BasicFonctionnalitiesDelegate.doAddProductAndReturnId(newProduct);
		Long referenceProductId = BasicFonctionnalitiesDelegate.doAddProductAndReturnId(anotherProduct);

		Buyer buyer = BasicFonctionnalitiesDelegate.doFindBuyerById(buyerId);
		Product product = BasicFonctionnalitiesDelegate.doFindProductById(productId);
		Product referenceProduct = BasicFonctionnalitiesDelegate.doFindProductById(referenceProductId);

		/* Create and persist some history of views */

		Date now = new Date(), later = new Date();
		later.setTime(later.getTime() + 1000);

		HistoryOfViews historyItem1 = new HistoryOfViews(randomString(), ClientType.other, buyer, product, now);
		HistoryOfViews historyItem2 = new HistoryOfViews(randomString(), ClientType.other, buyer, product, later);
		HistoryOfViews referenceHistoryItem = new HistoryOfViews(randomString(), ClientType.other, buyer,
				referenceProduct, now);

		BasicFonctionnalitiesDelegate.doAddHistoryOfViews(historyItem1);
		BasicFonctionnalitiesDelegate.doAddHistoryOfViews(historyItem2);
		BasicFonctionnalitiesDelegate.doAddHistoryOfViews(referenceHistoryItem);

		/* Create and persist some orders */

		OrderAndReview order1 = new OrderAndReview(15f, "Cascade package the items carefuly !", false, false, now,
				false, false, null, null, null, buyer, product);

		OrderAndReview order2 = new OrderAndReview(15f, "Cascade send in a strong cardoard box !", false, false, later,
				false, false, null, null, null, buyer, product);

		OrderAndReview referenceOrder = new OrderAndReview(15f, "Cascade please declare amount on customs form !",
				false, false, now, false, false, null, null, null, buyer, referenceProduct);

		BasicFonctionnalitiesDelegate.doAddOrderAndReview(order1);
		BasicFonctionnalitiesDelegate.doAddOrderAndReview(order2);
		BasicFonctionnalitiesDelegate.doAddOrderAndReview(referenceOrder);

		BasicFonctionnalitiesDelegate.doDeleteProduct(product);
	}

	@Test
	public void testDeleteSellerWithProductsAndHistoryAndOrders() {

		/* Create and Persist a Seller */

		Seller newSeller = new Seller("Control First Name", "Control Last Name", "CascadeSellerEmail@email.com",
				"21623000010", new Date("17/05/1991"), "Tunisia", true, false, "", "", 0f, 0f, "", 0f, false, "");
		Seller anotherSeller = new Seller("Control First Name", "Control Last Name", "ControlSellerEmail@email.com",
				"21623000011", new Date("17/05/1991"), "Tunisia", true, false, "", "", 0f, 0f, "", 0f, false, "");

		Long idSeller = BasicFonctionnalitiesDelegate.doAddSellerAndReturnId(newSeller);
		Long idAnotherSeller = BasicFonctionnalitiesDelegate.doAddSellerAndReturnId(anotherSeller);

		// System.out.println(" >>>>>>> idSeller : " + idSeller + " - idAnotherSeller : " + idAnotherSeller);

		Seller seller = BasicFonctionnalitiesDelegate.doFindSellerById(idSeller);
		Seller referenceSeller = BasicFonctionnalitiesDelegate.doFindSellerById(idAnotherSeller);

		/* Create and Persist a Buyer and a Product */

		Buyer newBuyer = new Buyer("Cascade First Name", "Cascade Last Name", "JustABuyer@email.com", "21623000000",
				new Date("17/05/1991"), "Tunisia", true, false, "", "", true, 0f, "No where street", "", "No City", 0f,
				0f);

		Product newProduct = new Product("Cascade product name", "Cascade product image url",
				"Cascade product description", 10f, "Cascade product description", true, true, 10);

		Product referenceProduct = new Product("Cascade Control product name", "Cascade Control product image url",
				"Cascade Control product description", 10f, "Cascade Control product description", true, true, 10);

		newProduct.setSeller(seller);
		referenceProduct.setSeller(referenceSeller);

		Long buyerId = BasicFonctionnalitiesDelegate.doAddBuyerAndReturnId(newBuyer);

		Long productId = BasicFonctionnalitiesDelegate.doAddProductAndReturnId(newProduct);
		BasicFonctionnalitiesDelegate.doAddProduct(referenceProduct);

		Buyer buyer = BasicFonctionnalitiesDelegate.doFindBuyerById(buyerId);
		Product product = BasicFonctionnalitiesDelegate.doFindProductById(productId);

		/* Create and persist some history of views */

		Date now = new Date(), later = new Date();
		later.setTime(later.getTime() + 1000);

		HistoryOfViews historyItem1 = new HistoryOfViews(randomString(), ClientType.other, buyer, product, now);
		HistoryOfViews historyItem2 = new HistoryOfViews(randomString(), ClientType.other, buyer, product, later);

		BasicFonctionnalitiesDelegate.doAddHistoryOfViews(historyItem1);
		BasicFonctionnalitiesDelegate.doAddHistoryOfViews(historyItem2);

		/* Create and persist some orders */

		OrderAndReview order1 = new OrderAndReview(15f, "Cascade package the items carefuly !", false, false, now,
				false, false, null, null, null, buyer, product);

		OrderAndReview order2 = new OrderAndReview(15f, "Cascade send in a strong cardoard box !", false, false, later,
				false, false, null, null, null, buyer, product);

		BasicFonctionnalitiesDelegate.doAddOrderAndReview(order1);
		BasicFonctionnalitiesDelegate.doAddOrderAndReview(order2);
		
		/* Delete a seller, make sure no jdbc exceptions on delete */
		
//		BasicFonctionnalitiesDelegate.doDeleteSeller(seller);

	}

}