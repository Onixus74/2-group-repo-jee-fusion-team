/*
 * 
 * Author Marwen
 */

package tn.edu.esprit.sigma.fusion.youbayclient.processing.delegate;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Buyer;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Seller;
import tn.edu.esprit.sigma.fusion.youbay.processing.SubscriptionServicesRemote;
import tn.edu.esprit.sigma.fusion.youbayclient.locator.ServiceLocator;

public class SubscriptionServicesDelegate {
	public final static String moduleName="youbay-war";
	
	private static final String jndiNameSubscriptionServicesRemote = moduleName+"/SubscriptionServices!tn.edu.esprit.sigma.fusion.youbay.processing.SubscriptionServicesRemote";
	
	private static SubscriptionServicesRemote subscriptionServicesGetProxy() {
		return (SubscriptionServicesRemote) ServiceLocator.getInstance().getProxy(
				jndiNameSubscriptionServicesRemote);
	}
	
	public static void doSubscribeSeller(Seller seller) {

		subscriptionServicesGetProxy().subscribeSeller(seller);
	}
	
	public static void doSubscribeBuyer(Buyer buyer) {

		subscriptionServicesGetProxy().subscribeBuyer(buyer);
	}

}
