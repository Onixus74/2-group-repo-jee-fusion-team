package tn.edu.esprit.sigma.fusion.youbayclient.payment;

import net.authorize.Environment;
import net.authorize.api.contract.v1.ARBSubscriptionType;
import net.authorize.api.contract.v1.ARBUpdateSubscriptionRequest;
import net.authorize.api.contract.v1.ARBUpdateSubscriptionResponse;
import net.authorize.api.contract.v1.CreditCardType;
import net.authorize.api.contract.v1.MerchantAuthenticationType;
import net.authorize.api.contract.v1.MessageTypeEnum;
import net.authorize.api.contract.v1.PaymentType;
import net.authorize.api.controller.ARBUpdateSubscriptionController;
import net.authorize.api.controller.base.ApiOperationBase;

public class UpdateSubscription {

	public static void run(String apiLoginId, String transactionKey) {
		// Common code to set for all requests
		ApiOperationBase.setEnvironment(Environment.SANDBOX);
		MerchantAuthenticationType merchantAuthenticationType = new MerchantAuthenticationType();
		merchantAuthenticationType.setName(apiLoginId);
		merchantAuthenticationType.setTransactionKey(transactionKey);
		ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

		// Populate the payment data
		PaymentType paymentType = new PaymentType();
		CreditCardType creditCard = new CreditCardType();
		creditCard.setCardNumber("4111111111111111");
		creditCard.setExpirationDate("1220");
		paymentType.setCreditCard(creditCard);

		ARBSubscriptionType arbSubscriptionType = new ARBSubscriptionType();
		arbSubscriptionType.setPayment(paymentType);

		// Make the API Request
		ARBUpdateSubscriptionRequest apiRequest = new ARBUpdateSubscriptionRequest();
		apiRequest.setSubscriptionId("100748");
		apiRequest.setSubscription(arbSubscriptionType);
		ARBUpdateSubscriptionController controller = new ARBUpdateSubscriptionController(apiRequest);
		controller.execute();
		ARBUpdateSubscriptionResponse response = controller.getApiResponse();
		if (response != null) {

			if (response.getMessages().getResultCode() == MessageTypeEnum.OK) {

				System.out.println(response.getMessages().getMessage().get(0).getCode());
				System.out.println(response.getMessages().getMessage().get(0).getText());
			} else {
				System.out.println("Failed to update Subscription:  " + response.getMessages().getResultCode());
			}
		}
	}
}