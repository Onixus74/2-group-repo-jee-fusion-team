package tn.edu.esprit.sigma.fusion.youbayclient.basicfonctionnalities.delegate;

import java.math.BigDecimal;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Buyer;
import tn.edu.esprit.sigma.fusion.youbay.utilities.MessagingServicesRemote;
import tn.edu.esprit.sigma.fusion.youbayclient.locator.ServiceLocator;
import tn.edu.esprit.sigma.fusion.youbayclient.payment.ChargeCreditCard;

public class ProcessingDelegate {
	public final static String moduleName = "youbay-war";
	public static final String jndiName = moduleName
			+ "/MessagingServices!tn.edu.esprit.sigma.fusion.youbay.utilities.MessagingServicesRemote";

	public static MessagingServicesRemote getProxy() {
		return (MessagingServicesRemote) ServiceLocator.getInstance().getProxy(jndiName);
	}

	public static void doSendEmail(String to, String subject, String msg) {
		getProxy().sendEmail(to, subject, msg);
	}

	public static void doChargeCreditCard() {
		// These are default transaction keys.
		// You can create your own keys in seconds by signing up for a sandbox
		// account here: https://developer.authorize.net/sandbox/
		final String apiLoginId = "9Bvezt7X4ra";
		final String transactionKey = "8BbwQ7Mt2rA23M26";
		// Update the payedId with which you want to run the sample code
		// final String payerId = "10";
		// Update the transactionId with which you want to run the sample code
		// final String transactionId = "2241801682";
		ChargeCreditCard.run(apiLoginId, transactionKey);
	}

	public static void doChargeCreditCardForBuyer(BigDecimal amount, Buyer buyer, String CardNumber,
			String ExpirationDate) {
		// These are default transaction keys.
		// You can create your own keys in seconds by signing up for a sandbox
		// account here: https://developer.authorize.net/sandbox/
		final String apiLoginId = "9Bvezt7X4ra";
		final String transactionKey = "8BbwQ7Mt2rA23M26";
		// Update the payedId with which you want to run the sample code
		// final String payerId = "10";
		// Update the transactionId with which you want to run the sample code
		// final String transactionId = "2241801682";
		ChargeCreditCard.run(apiLoginId, transactionKey, amount, buyer, CardNumber, ExpirationDate);
	}
}