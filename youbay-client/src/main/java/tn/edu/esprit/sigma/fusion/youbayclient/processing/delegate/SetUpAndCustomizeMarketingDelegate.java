//package tn.edu.esprit.sigma.fusion.youbayclient.processing.delegate;
//
//import java.util.List;
//
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.AssistantItems;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Auction;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Buyer;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Category;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.CustomizedAds;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.HistoryOfViews;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.HistoryOfViewsId;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Manager;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.OrderAndReview;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.OrderAndReviewId;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Product;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.ProductHistory;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Seller;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.SpecialPromotion;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Subcategory;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.AssistantItemsServicesRemote;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.AuctionServicesRemote;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.BuyerServicesRemote;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.CategoryServicesRemote;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.CustomizedAdsServicesRemote;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.HistoryOfViewsServicesRemote;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.ManagerServicesRemote;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.OrderAndReviewServicesRemote;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.ProductHistoryServicesRemote;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.ProductServicesRemote;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.SellerServicesRemote;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.SpecialPromotionServicesRemote;
//import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.SubcategoryServicesRemote;
//import tn.edu.esprit.sigma.fusion.youbay.processing.SetUpAndCustomizeMarketingRemote;
//import tn.edu.esprit.sigma.fusion.youbayclient.locator.ServiceLocator;
//
//public class SetUpAndCustomizeMarketingDelegate {
//	/*
//	 * Assistant Items Services Remote
//	 */
//	private static final String jndiNameSetUpAndCustomizeMarketingRemote = "youbay-ejb/SetUpAndCustomizeMarketing!tn.edu.esprit.sigma.fusion.youbay.processing.SetUpAndCustomizeMarketingRemote";
//
//	private static SetUpAndCustomizeMarketingRemote setUpAndCustomizeMarketingRemoteGetProxy() {
//		return (SetUpAndCustomizeMarketingRemote) ServiceLocator.getInstance()
//				.getProxy(jndiNameSetUpAndCustomizeMarketingRemote);
//	}
//}