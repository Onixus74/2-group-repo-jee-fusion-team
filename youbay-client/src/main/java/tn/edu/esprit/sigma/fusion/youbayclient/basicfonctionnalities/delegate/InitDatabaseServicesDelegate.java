package tn.edu.esprit.sigma.fusion.youbayclient.basicfonctionnalities.delegate;

import tn.edu.esprit.sigma.fusion.youbay.utilities.InitDatabaseServicesRemote;
import tn.edu.esprit.sigma.fusion.youbayclient.locator.ServiceLocator;

public class InitDatabaseServicesDelegate {
	public final static String moduleName="youbay-war";
	public static final String jndiName = moduleName+"/InitDatabaseServices!tn.edu.esprit.sigma.fusion.youbay.utilities.InitDatabaseServicesRemote";

	public static InitDatabaseServicesRemote getProxy() {
		return (InitDatabaseServicesRemote) ServiceLocator.getInstance()
				.getProxy(jndiName);
	}

	public static void doTruncateAllTables() {
		getProxy().truncateAllTables();
	}

	public static void doPopulateDatabase() {
		getProxy().populateDatabase();
	}

}