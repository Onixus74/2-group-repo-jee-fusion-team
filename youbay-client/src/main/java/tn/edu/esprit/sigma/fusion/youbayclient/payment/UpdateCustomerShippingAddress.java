package tn.edu.esprit.sigma.fusion.youbayclient.payment;

import net.authorize.Environment;
import net.authorize.api.contract.v1.CustomerAddressExType;
import net.authorize.api.contract.v1.MerchantAuthenticationType;
import net.authorize.api.contract.v1.MessageTypeEnum;
import net.authorize.api.contract.v1.UpdateCustomerShippingAddressRequest;
import net.authorize.api.contract.v1.UpdateCustomerShippingAddressResponse;
import net.authorize.api.controller.UpdateCustomerShippingAddressController;
import net.authorize.api.controller.base.ApiOperationBase;

public class UpdateCustomerShippingAddress {

	public static void run(String apiLoginId, String transactionKey) {

		ApiOperationBase.setEnvironment(Environment.SANDBOX);

		MerchantAuthenticationType merchantAuthenticationType = new MerchantAuthenticationType();
		merchantAuthenticationType.setName(apiLoginId);
		merchantAuthenticationType.setTransactionKey(transactionKey);
		ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);

		CustomerAddressExType customer = new CustomerAddressExType();
		customer.setFirstName("John");
		customer.setLastName("Doe");
		customer.setAddress("123 Main St.");
		customer.setCity("Bellevue");
		customer.setState("WA");
		customer.setZip("98004");
		customer.setCountry("USA");
		customer.setPhoneNumber("000-000-0000");
		customer.setCustomerAddressId("30000");

		UpdateCustomerShippingAddressRequest apiRequest = new UpdateCustomerShippingAddressRequest();
		apiRequest.setCustomerProfileId("10000");
		apiRequest.setAddress(customer);

		UpdateCustomerShippingAddressController controller = new UpdateCustomerShippingAddressController(apiRequest);
		controller.execute();

		UpdateCustomerShippingAddressResponse response = new UpdateCustomerShippingAddressResponse();
		response = controller.getApiResponse();

		if (response != null) {

			if (response.getMessages().getResultCode() == MessageTypeEnum.OK) {

				System.out.println(response.getMessages().getMessage().get(0).getCode());
				System.out.println(response.getMessages().getMessage().get(0).getText());
			} else {
				System.out.println(
						"Failed to update customer shipping address:  " + response.getMessages().getResultCode());
			}
		}
	}
}